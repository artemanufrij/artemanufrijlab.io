// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueResource from 'vue-resource';
import App from './App'
import 'bulma/css/bulma.css'
import router from './router.js'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faAngleDown, faToggleOff, faToggleOn, faClipboard, faLanguage, faCheck } from '@fortawesome/free-solid-svg-icons'
import { faGithub, faGitlab, faGooglePlus} from '@fortawesome/free-brands-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

Vue.config.productionTip = false

library.add(faToggleOff, faToggleOn, faClipboard, faLanguage, faCheck, faAngleDown, faGithub, faGitlab, faGooglePlus);

Vue.component('font-icon', FontAwesomeIcon)
Vue.use (VueResource);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<router-view/>'
})
