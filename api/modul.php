<?php
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: GET");
header("Content-Type: application/json; charset=utf-8");

$return_value = "[\n\t";
$i = 0;
    foreach (glob("../modules/*") as $filename) {
        if ($i > 0 ) {
                $return_value.="\n\t,";
        }
        $return_value = $return_value."{\n\t\"title\": \"".basename ($filename)."\"";
        $favicon = $filename."/static/favicon.png";
        if (file_exists ($favicon)) {
                $return_value = $return_value.",\"icon\": \"".$favicon."\"";
        }
        $return_value = $return_value."\n\t}";
        $i++;
    }
$return_value = $return_value."\n]";
echo $return_value;
?>
